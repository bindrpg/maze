# Maze

In James Dashner's *"The Maze Runner"* inspired adventure module players strive to survive in a place surrounded by a deadly labyrinth.

## Scale

## Theme

## Background

Baileys can survive only due to a nearby city, and the extra resources by an overseer.
In time, they form a protective ring around an area large enough to farm safely, with a city in the centre.

However, if that city falls to some calamity, the baileys generally fall with it.
The farmers there either leave, or the forest eats them.
People call these stone-shells 'lost cities'.
They don't call the baileys anything, as they're made of wood, and rot to nothing soon enough.

The *Glade*, however, survived.
The remaining people stayed and formed a small self-sufficient community.
They built fortified stone walls high enough to protect them from predators, as nature slowly reclaims the surrounding area.

## Goal
Players seek to escape the deadly labyrinth and return to their normal lives.

### The Arrival
- Among other Night Guards, the troupe protects a trading caravan on its way to an eremitic place called the *Glade*.
- The Glade's sole trading route leads through a tunnel several miles in length.
- On their way through the tunnel the walls around them start crumbling and shaking.
- The whole caravan needs to hasten their journey for the last couple of hundred metres to escape before the tunnel collapses.
- At the exit they find themselves in the Glade.
- A state of emergency starts to prevail, as the Gladers lose their only trading route to exchange resources for rare alchemical ingredients found only in the Maze.
- Additionally, the tunnel collapse jailed them in their harmonious place, alongside foreigners.

### The Punishment
- One day two Runners return just as the Doors are closing.
- One of them is injured and gets carried by the other.
- If the troupe decides to run through the Door without being Runners they get punished by violating Glade rule number 1: Never go outside the Glade, unless you are a Runner.

### The Changing
- A Glader returns from the Maze and got stung by a Griever.
- Without the Grief Serum as an antidote the one stung will die.
- A side effect of the Grief Serum leads to a painful state of the Changing in which the person goes through hallucinations and seizures.
- After the Changing, some (mostly negative) memories return from the time before coming to the Glade.
- One of those returned memories accuse the troupe of having done something severely dreadful.
- This leads to a conflict between some Gladers and the troupe.

### The Code
- The patterns of the Maze repeat periodically.
- The troupe can find letters by placing the maps of the eight sections from each day over another.
- By examining a whole repetition period worth of maps, they discover that the letters form a code.
- The troupe can use this code to stop the Grievers from coming out the Griever Hole.
- The Griever Hole is hidden somewhere in the Maze.

## Locations

### The Glade
- A large area in which the Gladers live.
- Located in the centre of the Maze.
- Four large walls separate the Glade from the Maze.
- A large Door in the centre of each wall enables Gladers to enter the Maze.
- The Doors close every night and open again in the morning.

#### Map Room
- In this building without windows the Runners draw and analyse their maps of the Maze.

#### Homestead
- Located in the north-west of the Glade.
- Functions as the accommodation for the Gladers.
- At the back the Gladers built the Slammer which functions as a jail.

#### Gardens
- Located in the north-east of the Glade.
- The grassy area functions as place to grow crops as a food source.

#### Deadheads
- Located in the south-west of the Glade.
- The forest area contains a graveyard for deceased Gladers.

#### Blood House
- Located in the south-east of the Glade
- The large barn functions as a place to raise and slaughter livestock.

### The Maze
- The Maze surrounding the Glade consists of eight sections.
- Each section features a unique flora and fauna, only sharing the Grievers at night.
- Some sections might have extreme weather conditions such as heat, cold, wind, high humidity, low oxygen level, etc.
- The troupe needs to find one piece of the code in each section.

#### The Lost City
- Once, the Glade surrounded a city as one of many baileys.
- As the city fell, a lich soon haunted the remaining inhabitants and gorged on their souls, while raising the corpses as an army of undead minions.
- In the wake of the sudden extinction, people deserted the city with most properties left behind.
- Over time plants overgrew the Lost City and nearly consumed all parts of it.

## Factions

### The Gladers

#### Jobs

The following job/role descriptions originate from the [Maze Runner Fandom Wiki](https://mazerunner.fandom.com/wiki/Roles).

##### Runners
Runners are the only ones who are allowed to go out into the Maze. They are defined as the strongest, fastest, and best of the Gladers. Runners run the Maze every day, trying to find a way out. The Maps they make are saved in the Map Room. They leave when the doors to the Maze open in the morning, and returned right before the doors close. They also have equipment like trainers and watches that are unavailable to the rest of the Gladers.

##### Builders
Builders are the ones who build most of the things around the Glade, such as structures and other simple items that could help with carrying and moving things around. They would also upgrade the structures and buildings.

##### Cooks
Cooks are the Gladers responsible for preparing meals for the Gladers. They spend most of their time in the kitchen. They make food from the work of the Blood Housers and Track-hoes.

##### Slicers
Slicers feed and raise the livestock such as pigs, hogs, cows, sheep, chickens, and turkeys, which are all kept in pens in the Blood House area. They also clean, fix fences, and scrape up clutter. They are the ones who slaughtered the animals, such as hogs, so they can be prepared by the Blood Housers and Cooks for eating. 

##### Blood Housers
Blood Housers take the meat of the slicers and prepare it before giving it to the cooks.

##### Track-hoes
Track-hoes do difficult work related to plants, such as trenching.

##### Gardeners
Gardeners prepare fruits and vegetables for the cooks. They take care of the plants in the Gardens: weeding, pruning, planting seeds, and harvesting veggies are examples of the things they do.

##### Med-jacks
Med-jacks are specialized Gladers who act as the equivalent to doctors in the Glade.

##### Sloppers
Sloppers are the Gladers who are not good at any of the jobs, so they help around the Glade mostly by doing the dirty tasks that other Gladers don't want to do, such as cleaning up the Blood House, kitchen, toilets, and showers.

##### Bricknicks
Bricknicks are the Gladers who had similar responsibilities to the Builders, but are mainly tasked with repairing the structures instead of building them.

##### Baggers
Baggers are the ones in charge of burying dead Gladers. They also act as guards and police around the Glade.

##### Map-makers
Map-makers are the ones who make the Maps of the Maze with the runners. They spend their days studying the maps while the runners run.

## Conflicts

## Mysteries/Secrets

## Extra Dangers

## Randomness

## Rewards
